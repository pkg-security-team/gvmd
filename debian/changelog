gvmd (24.0.0-1) unstable; urgency=medium

  * New upstream version 24.0.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 25 Nov 2024 15:40:33 +0100

gvmd (23.10.0-1) unstable; urgency=medium

  * New upstream version 23.10.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 09 Oct 2024 11:04:12 +0200

gvmd (23.8.1-1) unstable; urgency=medium

  * New upstream version 23.8.1
  * Add missing build-dep libcjson-dev
  * Bump Standards-Version to 4.7.0 (no change)

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 28 Aug 2024 13:57:52 +0200

gvmd (23.6.2-1) unstable; urgency=medium

  * New upstream version 23.6.2
  * Move files to /usr (Closes: #1073695)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 18 Jun 2024 17:47:38 +0200

gvmd (23.5.2-1) unstable; urgency=medium

  * New upstream version 23.5.2
  * Update minimal version of libgvm-dev
  * Replace build-dep pkg-config with pkgconf
  * Refresh patches
  * Remove hard-coded lib (Closes: #1069332)
  * Depends on correct package postgresql-VERSION-pg-gvm
  * Update minimal required version of greenbone-feed-sync

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 24 Apr 2024 08:58:35 +0200

gvmd (23.1.0-1) unstable; urgency=medium

  * Add nsis and rpm in Recommends
  * Add new package greenbone-feed-sync in dependency
  * Update debian/copyright
  * Update minimal version libgvm-dev
  * Do not install old sync scripts
  * Update debian/README.Debian for gvm 22.5
  * Refresh patches
  * New upstream version 23.1.0

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Nov 2023 10:25:41 +0100

gvmd (22.4.2-1) unstable; urgency=medium

  * New upstream version 22.4.2
  * Fix the postinst(s) (Closes: #1029624): put the command in the postinst of
    the correct package
  * Bump Standards-Version to 4.6.2 (no changes)

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 26 Jan 2023 11:24:58 +0100

gvmd (22.4.1-1) unstable; urgency=medium

  [ Sophie Brun ]
  * New upstream version 22.4.0
  * Refresh patches
  * Update debian/copyright
  * Update build-deps
  * Add depends pg-gvm
  * Remove old patches (for old migrations)
  * Update deps: switch to libgvm22
  * Add missing dep notus-scanner
  * Update minimal version of ospd-openvas
  * Fix pre-installed files for export pdf
  * New upstream version 22.4.1
  * Refresh patches
  * Fix build with new gpgme (Closes: #1024810)
  * Remove useless depends on obsolete package lsb-base

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.
  * Remove constraints unnecessary since buster (oldstable)

  [ Heinrich Schuchardt ]
  * Fix buffer overrun in buffer_aggregate_xml()

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 23 Jan 2023 10:26:19 +0100

gvmd (21.4.5-2) unstable; urgency=medium

  * Add a patch to fix the regex for the new glib2.0: it was breaking the
    validation of the username in gvmd (see
    https://bugs.kali.org/view.php?id=7926).

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 21 Sep 2022 09:11:57 +0200

gvmd (21.4.5-1) unstable; urgency=medium

  * Define GVM_DEFAULT_DROP_USER as _gvm
  * New upstream version 21.4.5
  * Update debian/copyright
  * Adapt gvmd.service for new runtime dir: gmvd (instead of gvm)
  * Refresh patches
  * Add minimal required version of libgvm
  * Add missing depends ospd-openvas
  * wrap and sort
  * Improve the README.Debian

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 03 May 2022 16:18:43 +0200

gvmd (21.4.4-1) unstable; urgency=medium

  * New upstream version 21.4.4
  * Update debian/rules
  * Refresh patches
  * Install /lib/systemd/system/gvmd.service
  * Bump Standards-Version to 4.6.0 (no changes)
  * Add manpage for greenbone-feed-sync

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 04 Jan 2022 09:46:20 +0100

gvmd (21.4.3-1) unstable; urgency=medium

  * Update minimal required version of libgvm-dev
  * Update debian/copyright
  * New upstream version 21.4.3
  * Update minimal required version of libgvm-dev to 21.4.0
  * Import upstream patch to fix DB migration
  * Update build options
  * Update packages in Suggests
  * Refresh patches
  * Bump minimal required version of libgvm
  * Change installation of gvmd.service

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 30 Aug 2021 15:57:51 +0200

gvmd (20.8.0+git20201111-2) unstable; urgency=medium

  * Team upload.

  [ Sophie Brun ]
  * Remove old and obsolete debian/gvmd.init
  * Add a patch to not install /etc/default/gvmd

 -- Raphaël Hertzog <raphael@offensive-security.com>  Wed, 16 Dec 2020 17:55:13 +0100

gvmd (20.8.0+git20201111-1) unstable; urgency=medium

  * Team upload.

  [ SZ Lin (林上智) ]
  * I am no longer using this package, and thus remove my name from the
    uploaders field due to time constraints

  [ Sophie Brun ]
  * New upstream version 20.8.0+git20201111
  * Change Upstream license: now AGPL-3+
  * Set GVM_FEED_LOCK_PATH in debian/rules
  * Add xsltproc in Depends
  * Refresh patches
  * Use Upstream config files
  * Use /run directly, not /var/run
  * Don't use EnvironmentFile in gvmd.service
  * Upddate minimal required version of libgvm-dev
  * Fix change-lock-location.patch
  * Generate versioned dependency in postgresql
  * Remove old debian/TODO
  * Add a patch to allow upgrade from previous version

  [ Raphaël Hertzog ]
  * Bump Standards-Version to 4.5.1

 -- Raphaël Hertzog <raphael@offensive-security.com>  Thu, 03 Dec 2020 15:35:24 +0100

gvmd (9.0.1-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/tests: Set -DPostgreSQL_TYPE_INCLUDE_DIR=/usr/include/postgresql as
    debian/rules already does. Fixes test failure with PostgreSQL 13.
    (Closes: #971773)

 -- Christoph Berg <myon@debian.org>  Wed, 14 Oct 2020 20:45:36 +0200

gvmd (9.0.1-4) unstable; urgency=medium

  * Team upload.

  [ Sophie Brun ]
  * Fix the postrm for the purge

  [ Raphaël Hertzog ]
  * Enable diffoscope for reprotest CI check
  * Apply a patch to make the package build reproducible
  * Allow reprotest failure in CI

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 28 Aug 2020 17:01:41 +0200

gvmd (9.0.1-3) unstable; urgency=medium

  * Team upload.

  [ Sophie Brun ]
  * store the postgresql version for check in gvm package

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 07 Aug 2020 21:22:43 +0200

gvmd (9.0.1-2) unstable; urgency=medium

  * Team upload.

  [ Sophie Brun ]
  * Fix SQL query in Fix-gvm-migrate-to-postgres.patch (Closes: #966632)
  * Update gvmd.service to start after ospd-openvas

 -- Raphaël Hertzog <raphael@offensive-security.com>  Thu, 06 Aug 2020 14:54:15 +0200

gvmd (9.0.1-1) unstable; urgency=medium

  * Team upload

  [ Sophie Brun ]
  * Import upstream patch to fix db migration
  * Update debian/README.Debian

  [ Raphaël Hertzog ]
  * Remove obsolete field Name from debian/upstream/metadata (already present
    in machine-readable debian/copyright).
  * Get rid of /var/lib/gvm/{private,}/CA during purge (Closes: #905518)
  * Extend descriptions of patches

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 31 Jul 2020 17:04:50 +0200

gvmd (9.0.1-1~exp1) experimental; urgency=medium

  [ Sophie Brun ]
  * Switch to gvmd
  * New upstream version 9.0.0
  * Switch all debian/* to gvmd
  * Update docs installation
  * Add missing DPostgreSQL_TYPE_INCLUDE_DIR in debian/rules
  * Add a patch to fix small spelling error
  * Update debian/gvmd.service
  * Update debian/copyright
  * Update Vcs-* fields for gvmd
  * Import upstream patch to fix gvm-portnames-update
  * Use postgres in gvm-portnames-update
  * Bump Standards-Version to 4.5.0
  * Use debhelper-compat 12
  * Use a special user _gvm and change GVM_RUN_DIR
  * Don't start service after installation
  * Add /var/lib/gvm/private directory
  * Update gvmd.service to use ospd
  * New upstream version 9.0.1
  * Update minimal required version of libgvm11
  * Improve gvmd.service
  * Add permissions to /var/log/gvm and /var/lib/gvm
  * Remove change-GVM_RUN_DIR.patch, use parameter to change GVM_RUN_DIR
  * Fix debian/gvmd.logrotate with user _gvm and pid location
  * Add missing dependency: postgresql
  * Add a patch to change lockfile location

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian

  [ SZ Lin (林上智) ]
  * Add autopkgtest test case
  * Add patch for removing tests of removed functions
  * Move adduser package from B-D to D for postinst usage
  * Update copyright information
  * Update uploaders information
  * Set Rules-Requires-Root: no
  * Bump debhelper-compat to 13
  * Add upstream metadata

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 03 Jul 2020 10:43:27 +0800

openvas-manager (7.0.3-1) unstable; urgency=medium

  [ Sophie Brun ]
  * Team upload.
  * Update debian/watch and debian/control with new upstream github repository
  * Import new usptream release
  * Update date in debian/copyright
  * Update minimal required version of libopenvas-dev
  * Add texlive-latex-extra in Recommends (see
    https://bugs.kali.org/view.php?id=4839)

  [ Raphaël Hertzog ]
  * Drop the patch, it has been merged upstream.
  * Bump Standards-Version to 4.1.5.

 -- Sophie Brun <sophie@freexian.com>  Fri, 06 Jul 2018 14:57:27 +0200

openvas-manager (7.0.2-4) unstable; urgency=medium

  * Team upload.
  * Revert "Replace insecure URI to secure URI in d/watch"
  * Clean up rules file
    - use dh_missing --fail-missing instead of dh_install's option
    - move some commands to their logical place (fix permissions in
      override_dh_fixperms, dropping installed files right after their
      installation)
  * Add usual values to openvas-manager.init's Default-Start field
  * Drop debian/.git-dpm, we don't use that tool
  * Install upstream's README properly
  * Drop debian/openvas-manager.manpages since manual pages are installed by
    cmake
  * Install files from /usr/share/man/man1/ in openvas-manager-common too
  * Automatically setup the required certificate files in the postinst

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 06 Apr 2018 00:04:52 +0200

openvas-manager (7.0.2-3) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org
  * Update team maintainer address to Debian Security Tools

  [ Helmut Grohne ]
  * Drop unused Build-Depends: flawfinder. (Closes: #893918)

  [ SZ Lin (林上智) ]
  * d/control: Bump standards version to 4.1.3
  * d/control: Bump version of "debhelper" to 11
  * d/compat: Bump compat version to 11
  * d/watch: Replace insecure URI to secure URI
  * d/copyright: Replace insecure URI to secure URI

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 04 Apr 2018 21:51:26 +0800

openvas-manager (7.0.2-2) unstable; urgency=medium

  [ Javier Fernández-Sanguino Peña ]
  * Merge previous changelog entries from the package that were in
    experimental some years ago (Closes: #873967)

  [ SZ Lin (林上智) ]
  * d/control: Bump standards version to 4.1.2
  * d/control: Bump version of "debhelper" to 10
  * d/compat: Bump compat version to 10
  * d/rules: Remove unnecessary parameter in dh
  * Import patch from upstream to fix scheduled task failing issue
    (Closes: #868123)

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 01 Dec 2017 14:30:20 +0800

openvas-manager (7.0.2-1) unstable; urgency=medium

  * Import new upstream release

 -- SZ Lin (林上智) <szlin@debian.org>  Thu, 29 Jun 2017 17:10:43 +0800

openvas-manager (7.0.1-3) unstable; urgency=medium

  * Fix FTBFS issue (Closes: #865291)

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 20 Jun 2017 23:35:04 +0800

openvas-manager (7.0.1-2) unstable; urgency=medium

  * Move package from experimental to sid archive
  * Bump standards version to 4.0.0

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 20 Jun 2017 11:28:21 +0800

openvas-manager (7.0.1-1~exp1) experimental; urgency=medium

  * Import new upstream release

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 22 Mar 2017 20:17:38 +0800

openvas-manager (7.0.0-1) UNRELEASED; urgency=medium

  * Import new upstream release

 -- SZ Lin (林上智) <szlin@cs.nctu.edu.tw>  Mon, 14 Nov 2016 14:44:57 +0800

openvas-manager (6.0.9-2) unstable; urgency=high

  * Split override_dh_install target out into override_dh_install-indep
    and override_dh_install-arch (Closes: #843161)

 -- SZ Lin (林上智) <szlin@cs.nctu.edu.tw>  Sat, 05 Nov 2016 00:57:17 +0800

openvas-manager (6.0.9-1) unstable; urgency=medium

  * Import upstream version to Debian (Closes: #840081)

 -- SZ Lin (林上智) <szlin@cs.nctu.edu.tw>  Sat, 08 Oct 2016 23:17:16 +0800

openvas-manager (6.0.8-0kali1) kali-experimental; urgency=medium

  * Import new upstream release
  * Update minimal required version of libopenvas

 -- Sophie Brun <sophie@freexian.com>  Wed, 22 Jun 2016 11:27:47 +0200

openvas-manager (6.0.5-0kali1) kali-dev; urgency=medium

  * Import new upstream release.

 -- Sophie Brun <sophie@freexian.com>  Fri, 11 Sep 2015 08:56:32 +0200

openvas-manager (6.0.1-0kali2) kali-dev; urgency=medium

  * Add a proper systemd service file.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 28 Jul 2015 16:57:37 +0200

openvas-manager (6.0.1-0kali1) kali; urgency=medium

  * Import new upstream release
  * openvas-manager.init: drop command line with --slisten and --sport as they
    don't exist anymore (it's managed by openvas-scanner)

 -- Sophie Brun <sophie@freexian.com>  Fri, 10 Apr 2015 16:04:11 +0200

openvas-manager (5.0.2-0kali3) kali-dev; urgency=medium

  * Rebuild in kali-dev for the libgcrypt20/gnutls28 transitions.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 21 Nov 2014 21:22:43 +0100

openvas-manager (5.0.2-0kali2) kali; urgency=medium

  * Add Replaces: libopenvas6 due to package taking over
    /usr/share/openvas/openvas-lsc-rpm-creator.sh
  * Don't start/stop/restart the service in maintainer scripts as they
    don't work well when the package is not configured yet.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 26 Aug 2014 10:31:22 +0200

openvas-manager (5.0.2-0kali1) kali; urgency=medium

  * New upstream release.
     - Add support for multiple "To" addresses in Alerts.
     - Added Chinese as allowed value for interface language.
     - Ensure timestamps are based on the UTC timezone in the synchronisation
       scripts.
     - Consistently check the return values in the synchronisation scripts.
     - Separate init for command options like --create-user into new function,
       otherwise these options affect active tasks.
     - Fix issues around report format deletion.
  * Add missing files debian/copyright and debian/source/format
  * Update debian/rules to use debhelper 9
  * Add build depend: flawfinder
  * Add "Default stop" 1 in .init
  * Add lintian-overrides

 -- Sophie Brun <sophie@freexian.com>  Wed, 30 Jul 2014 14:32:04 +0200

openvas-manager (4.0.4-0kali1) kali; urgency=low

  * New upstream release with security fixes:
    http://permalink.gmane.org/gmane.comp.security.full-disclosure/90938

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 18 Nov 2013 09:42:04 +0100

openvas-manager (4.0.0-0kali1) kali; urgency=low

  * New upstream release.
  * Enable dh_install --list-missing and disable verbose mode.
  * Update build dependencies and list of installed files.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 02 May 2013 11:50:33 +0200

openvas-manager (3.0.4-1kali0) kali; urgency=low

  [ Stephan Kleine ]
  * New upstream release.
    - Support for note lifetimes has been added.
    - A bug which caused an incorrect result count in the Asset Management view has
      been fixed.
    - Support for displaying the hostname and the note and override lifetimes has
      been added to the following report formats: HTML, LaTeX and TXT.

  [ Mati Aharoni ]
  * Version update

 -- Mati Aharoni <muts@kali.org>  Tue, 04 Dec 2012 13:49:38 -0500

openvas-manager (3.0+beta7-1) UNRELEASED; urgency=low

  * New upstream release.
    - The handling of timestamps in reports has been improved.
    - Report format signatures no longer contain user editable fields.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 24 Nov 2011 23:18:29 +0100

openvas-manager (3.0+beta6-1) UNRELEASED; urgency=low

  * New upstream release.
    - Support for listing NVTs addressing a certain CVE has been added.
    - A bug which cause the wrong threat level to be displayed for tasks with
      multiple reports under rare circumstances has been fixed.
    - Support for override lifetimes has been added.
    - Support for displaying the SCAP update timestamp has been added.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 17 Nov 2011 23:27:01 +0100

openvas-manager (3.0+beta5-1) UNRELEASED; urgency=low

  * New upstream release.
    - Support for asset management been added.
    - Support for using SCAP data directly has been added.
    - A support script for retrieving SCAP data has been added.
    - Support for prognostic scans has been added.
    - Support for large database files on 32-bit platforms has been improved.
    - Support for task observers has been added.
    - Support for individual time zones for users has been added.
    - A bug which caused the filter result count to be calculated incorrectly when
      searching for text phrases has been fixed.
    - Support for sorting results by CVSS score has been added.
    - Support for importing results sent through the XML escalator has been added.
    - The max_host and max_checks scan performance parameters have been moved from
      scan configs to tasks.
    - Support for delta reports has been added.
    - An issue which caused meta data for NVTs in the "Credentials" family to be not
      present in the database has been fixed.
    - A number of compiler warnings discovered by Stephan Kleine have been
      addressed.
    - A race condition which caused empty reports from the slave when running in
      master-slave mode under certain conditions has been fixed.
    - Scan start and end times are now set for imported reports.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Sat, 12 Nov 2011 19:38:48 +0100

openvas-manager (3.0+beta2-1) UNRELEASED; urgency=low

  * New upstream release.
    - An issue which caused the import of very large reports to fail under certain
      circumstances has been fixed.
    - An issue which caused the date of the scan end to be missing from scans if
      they were associated with an escalator created with an early Manager version
      has been fixed.
    - The handling of long host lists has been improved.
    - New feature: Trashcan. It is now possible to place objects in a trashcan
      instead of deleting them directly.
    - Support for Host Details reported by OpenVAS Scanner has been added.
    - Support for container task (imported reports) has been added.
    - Support for specifying an SSH port for Local Security Checks has been added.
    - Security: Enforces strict permissions on sensitive OpenVAS Manager files.
    - Security: Drop privileges before executing report format plugins if running with
      elevated privileges.
    - Security: Ensures report formats are trusted before executing them.
    - Support for escalating result to a Sourcefire Defense Center has been added.
    - Support for using an SSH key pair for SSH authentication has been added.
    - The mail addresses supplied for an email escalator are now used in the correct
      order.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Sat, 21 May 2011 22:07:47 +0200

openvas-manager (2.0.3-5) experimental; urgency=low

  * debian/openvas-manager.postinst: Use -n instead of -e in echo calls (typo)
  * debian/openvas-manager.{postinst,init}: Create the Database if it does not
     exist in the init.d script instead of in the postinst script.
  * debian/openvas-manager.preinst: Run openvasmd --migrate when upgrading
     the package.
  * debian/rules: change DATADIR to /usr/share

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Fri, 13 May 2011 21:14:59 +0200

openvas-manager (2.0.3-4) experimental; urgency=low

  * debian/control: Fix typos and adjust name for gsad and gsd
  * debian/openvas-manager.postrm:
      - Fix the location of the logs
      - Fix bashishms

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Wed, 04 May 2011 20:28:17 +0200

openvas-manager (2.0.3-3) experimental; urgency=low

  * debian/rules: set CMAKE_INSTALL_PREFIX to an empty variable and define
   the other variables with absolute (not relative) paths
  * debian/openvas-manager.postinst: Only create the user if the
    openvas-mkcert-client binary is available (provided by OpenVAS scanner)
  * debian/openvas-manager.postinst: Remove the kludge to cd into the root
    directory as this is no longer needed.

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Sun, 01 May 2011 09:23:50 +0200

openvas-manager (2.0.3-2) experimental; urgency=low

  * debian/openvas-manager.postinst: Change to the root directory before
   rebuilding the database since the location defined in the openvas-manager
   is, for some, reason, relative.

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Sun, 01 May 2011 08:58:49 +0200

openvas-manager (2.0.3-1) experimental; urgency=low

  * Initial release, heavily based on the openvas-scanner package.

  * Patches for upstream:
    - 10_cmake_mandir.dpatch: Define a MANDIR variable to use in the
      installation of manpages
    - 20_avoid_absolute_files.dpatch: Make location of report_formats relative
      to the installation directory

 -- Javier Fernandez-Sanguino Pen~a <jfs@debian.org>  Sat, 30 Apr 2011 11:53:03 +0200

openvas-manager (2.0.3-1) UNRELEASED; urgency=low

  * New upstream release.
    - Enforces strict permissions on sensitive OpenVAS Manager files.
    - Drop privileges before executing report format plugins if running with
      elevated privileges.
    - Ensures report formats are trusted before executing them.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 22 Apr 2011 12:13:53 +0200

openvas-manager (2.0.2-1) UNRELEASED; urgency=low

  * New upstream release.
    - The test infrastructure has been updated.
    - A bug which caused the host count to be calculated incorrectly under certain
      circumstances has been fixed.
    - A number of memory and resource leaks discovered by Felix Wolfsteller have
      been closed.
    - A bug which caused database migration to fail when upgrading very old
      installations has been fixed.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 03 Mar 2011 01:41:38 +0100

openvas-manager (2.0.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - Support for fallback system reports has been added in case where a full system
      report is unavailable.
    - The expected location for signatures has been updated to match the FHS related
      changes in OpenVAS Scanner.
    - The "unscanned_closed" preference now defaults to "yes" for predefined
      configs.
    - The report format signature infrastructure has been improved.
    - A bug which caused valid host names to be reject under certain circumstances
      has been fixed.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Wed, 23 Feb 2011 19:53:56 +0100

openvas-manager (2.0.0-1) UNRELEASED; urgency=low

  * New upstream release.
    - The logging behaviour when started with --verbose has been improved.
    - The w3af NVT is no longer part of the Full and fast default scan config.
    - The build environment has been cleaned up.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Wed, 09 Feb 2011 22:17:04 +0100

openvas-manager (1.99.4-1) UNRELEASED; urgency=low

  * New upstream release.
    - Documentation has been updated.
    - OpenVAS Manager now uses pkg-config to find required libraries.
    - An issue which caused the creation of overrides to fail under some
      circumstances has been fixed.
    - The installation is now compliant with Filesystem Hierarchy Standard (FHS
      2.3).

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 04 Feb 2011 21:30:54 +0100

openvas-manager (1.99.3-1) UNRELEASED; urgency=low

  * New upstream release.
    - A security-relevant bug has been fixed regarding email escalation methods.
      Configured OpenVAS users were able to damage installation and/or gain higher
      privileges.
    - An issue which caused database migration to fail under certain circumstances
      has been fixed.
    - The default log level has been reduced to warning to prevent the logging of
      potentially sensitive information in the default configuration.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Mon, 24 Jan 2011 14:54:44 +0100

openvas-manager (1.99.2-1) UNRELEASED; urgency=low

  * New upstream release.
    - The protocol documentation has been brought up to date.
    - The output of --version now complies with the GNU Coding Standards.
    - Passwords are now masked when loggings LSC package creation commands.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Mon, 10 Jan 2011 16:02:16 +0100

openvas-manager (1.99.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - The protocol documentation has been improved.
    - Target credentials for SMB and SSH are now separated.
    - Support for setting the port range for a target has been added.
    - Hardening flags are now enabled during compile time to increase code quality.
    - Support for retrieving the total number of results matching an applied filter
      has been added.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Tue, 21 Dec 2010 14:02:16 +0100

openvas-manager (1.98.3-1) UNRELEASED; urgency=low

  * New upstream release.
    - The protocol self-documentation has been improved.
    - NTLMSSP is now enabled by default.
    - Escalator message now include more information.
    - The LaTeX and PDF reports have been made more consistent with the other
      report formats.
    - An issue which caused internal links in the PDF report to link to wrong
      results under certain circumstances has been fixed.
    - An issue which caused some existing LSC credential packages to be empty on
      subsequent downloads under certain circumstances has been fixed.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 10 Dec 2010 14:48:27 +0100

openvas-manager (1.98.2-1) UNRELEASED; urgency=low

  * New upstream release.
    - The protocol self-documentation has been improved.
    - A number of superfluous log messages has been downgraded or removed.
    - A bug which caused issues to be counted incorrectly in the ports overview has
      been fixed.
    - The generation of PDF and LaTeX reports is now faster.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 03 Dec 2010 18:14:52 +0100

openvas-manager (1.98.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - Report Format plugin framework. All previous reporting features
      were converted to plugins. The XML representation of a report
      is now the base for any plugin and thus consistency of reports
      is improved.

      Report Format Plugins can be set active so that they
      appear in the selection lists. Selections can consider
      content types so that for example only the plugins with
      content type "text" are offered as Email body.

      It is possible to use parameters for the plugins so the
      user can adjust the behaviour of the Report Format to
      the individual preferences or needs.

      A verification method allows to distribute signatures
      for valid plugins via the NVT Feed.
    - New default Report Format: TXT for simple text.
    - New default Report Format: LaTeX for LaTeX source.
    - New sample Report Format: Simple Bar Chart.
      Demonstrates how to use Gnuplot for graphical reports.
    - New sample Report Format: Simple Topo Plot.
      Demonstrates how to use Graphviz for graphical reports.
    - New sample Report Format: Simple Pie Chart.
      Demonstrates how to use PyChart for graphical reports.
    - New sample Report Format: Simple Map Plot.
      Demonstrates how to use MapServer and GDAL for graphical reports.
    - New sample Report Format: Sourcefire Host Input.
    - Demonstrates that Report Formats can be used to build connectors.
    - Master-Slave feature. Any OpenVAS Manager can use one or many other
      OpenVAS Manager as slave to run scans. The whole scan task
      is transferred to the slave, results are continuously reported
      to the Master during scan process. After the scan is finished
      all data are removed from the slave.

      The master can also retrieve system reports from the slave and
      thus can collect the performance overview for all configured slaves.
    - New Escalator: HTTP GET. This allows for example to access
      text message (SMS) gateways or ticket management systems.
    - Extended Escalator: For EMail escalation it is now possible
      to select from configured Report Formats to be included in the
      Email body.
    - Agents: A verification method was added. This allows to
      distribute signatures for valid agents via the NVT Feed.
    - Credentials: Can now be edited. This allows to change the login
      name or password without the need to create a new scan configuration.
    - Credentials: Auto-generated installer packages are now created on
      the fly. If the generators are improved, it is now easy to create
      an updated package for already existing credentials.
    - OMP self-documentation: Part of the Managers' XML-based communication protocol
      OMP 2.0 is to deliver the full specification and documentation of the
      protocol itself (command "HELP"). It can be retrieved as XML-,
      RNC- or HTML representation.
    - Targets: Various opportunities have been added to specify and combine IP ranges.
    - Tasks: The task overview is delivered much faster now.
    - Reports: The report filtering is much faster now.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 19 Nov 2010 17:49:08 +0100

openvas-manager (1.0.3-1) UNRELEASED; urgency=low

  * New upstream release.
    - Two bugs which caused the manager to fail to give adequate replies on certain
      report and scan config requests have been fixed.
    - A bug which caused PDF reports to be unavailable for reports which contained
      certain unicode character has been fixed.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Sun, 31 Oct 2010 19:29:08 +0100

openvas-manager (1.0.2-1) UNRELEASED; urgency=low

  * New upstream release.
    - A bug which could cause changes in derived scan configs to affect predefined
      scan configs under certain circumstances has been fixed.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Tue, 17 Aug 2010 16:25:42 +0200

openvas-manager (1.0.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - A fix for incorrect preference values in the database has been adjusted to
      work with GSA 1.0.1 as well.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Mon, 09 Aug 2010 16:41:26 +0200

openvas-manager (1.0.0-1) UNRELEASED; urgency=low

  * New upstream release.
    - A number of build issues has been addressed.
    - The code documentation has been updated.
    - Code cleanup: Internal error handling has been made more consistent.
    - A potential ressource leak identified by static analysis has been fixed.
    - A bug which caused NVT preferences to be displayed incorrectly has been
      fixed.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 29 Jul 2010 15:40:02 +0200

openvas-manager (1.0.0.rc1-1) UNRELEASED; urgency=low

  * New upstream release.
    - Code cleanup: Internal resource management has been improved to use UUIDs in
      more places.
    - Support for agents has been improved.
    - Support for external target sources has been added.
    - A bug which caused PDF exports to fail if the NVT description contained
      certain characters has been fixed.
    - A bug which caused hosts in the scan result to be sorted incorrectly under
      certain circumstances has been fixed.
    - Support for defining threat overrides has been added.
    - Some OMP commands have been renamed and adjusted to make the protocol more
      concise and useful.
    - Support for event logging has been added.
    - Support for syslog escalators has been added.
    - The documentation has been updated.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 16 Jul 2010 18:04:30 +0200

openvas-manager (1.0.0.beta7-1) UNRELEASED; urgency=low

  * New upstream release.
    - A large amount of code which was present in both openvas-manager and
      openvas-administrator has been moved to openvas-libraries.
    - An issue that caused started tasks to remain in the "Requested" stage
      indenfinitely has been fixed.
    - An issue that caused incorrect values of the scan progress under certain
      conditions has been fixed.
    - A new escalator condition has been add: Threat Level Changed.
    - Open ports are now included in scan reports even if no vulnerability was
      detected on that port.
    - Support for CVSS scores and Risk Factors has been improved.
    - Support for excluding host without any results from the report has been added.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Sat, 29 May 2010 09:31:25 +0200

openvas-manager (1.0.0.beta6-1) UNRELEASED; urgency=low

  * New upstream release.
    - A bug which caused incorrect NVT counts in the scan config under certain
      circumstances has been fixed.
    - The manager now uses certificate based authentication.
    - Support for resuming stopped tasks has been added.
    - Support for task scheduling has been added.
    - The openvasmd binary will now install into /usr/sbin instead of /usr/bin.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 15 Apr 2010 17:59:16 +0200

openvas-manager (1.0.0.beta5-1) UNRELEASED; urgency=low

  * New upstream release.
    - More internal data structures are now identified by UUID and not by
      name.
    - Several build issues have been fixed.
    - Note management has been introduced.
    - Support for handling ITG and CPE reports has been added.
    - OTP forwarding is now disabled by default.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 04 Mar 2010 19:12:18 +0100

openvas-manager (1.0.0.beta4-1) UNRELEASED; urgency=low

  * New upstream release.
    - More internal data structures are now identified by UUID and not by name.
    - A bug which prevented PDF reports to be generated from certain results due to
      unescaped LaTeX characters has been fixed.
    - A number of formatting and casting issues found by Stephan Kleine have been
      fixed.
    - The man page has been updated.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Mon, 08 Feb 2010 12:21:06 +0100

openvas-manager (1.0.0.beta3-1) UNRELEASED; urgency=low

  * New upstream release.
    - Nmap is now the default port scanner for predefined configurations.
    - The man page has been updated.
    - LSC credential management has been improved.
    - A number of internal data structures are now identified by UUID and not by
      name.
    - The manager now converts all input from the scanner to UTF-8.
    - The encoding of the LaTeX report has been switch to UTF-8.
    - A bug that caused some settings to be ignored during scan configuration import
      has been fixed.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 05 Feb 2010 15:19:05 +0100

openvas-manager (1.0.0.beta2-1) UNRELEASED; urgency=low

  * New upstream release.
    - Deleting of active reports is prevented.
    - Introduced ownership for all objects.
      This makes objects (like a "target") not
      appear for other users anymore.
    - Improved ISO-8859-1 to UTF-8 conversion hacks.
    - Allowed "\" for login names (important for windows)
    - Send users host restrictions ("rules") via OTP when
      starting a scan.
    - Activated NSIS package generator for credentials management.
    - Filter out potentials passwords from logging.
    - Introduced UUIDs for users.
    - Improved PDF report generator.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Wed, 27 Jan 2010 11:49:33 +0100

openvas-manager (1.0.0.beta1-1) UNRELEASED; urgency=low

  * Initial package.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Sat, 23 Jan 2010 09:36:42 +0100
